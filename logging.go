package turntable

import (
	"io"
	"log/syslog"
)

type syslogWriterWithFallback struct {
	syslogWriter   io.Writer
	fallbackWriter io.Writer
}

func (w syslogWriterWithFallback) Write(p []byte) (n int, err error) {
	n, err = w.syslogWriter.Write(p)
	if err != nil {
		return w.fallbackWriter.Write(p)
	}
	return n, err
}

func SyslogWriter(addr string, tag string, fallback io.Writer) (io.Writer, error) {
	var err error
	var w *syslog.Writer
	priority := syslog.LOG_EMERG | syslog.LOG_KERN // default priority
	// return a syslog writer
	if addr != "" { // either through establishing a connection
		w, err = syslog.Dial("udp", addr, priority, tag)
	} else { // or using the system log daemon
		w, err = syslog.New(priority, tag)
	}
	return syslogWriterWithFallback{
		syslogWriter:   w,
		fallbackWriter: fallback,
	}, err
}
