package turntable

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

func ReceiveRequest(w http.ResponseWriter, r *http.Request) {
	var err error
	tag := os.Getenv("PROJECT_TAG")
	// Setup logging
	var loggerOutput io.Writer
	syslogAddr := os.Getenv("SYSLOG_ADDR")
	if syslogAddr != "" {
		loggerOutput, err = SyslogWriter(syslogAddr, tag, os.Stdout)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		loggerOutput = os.Stdout
	}
	var body []byte
	if r.Body != nil {
		body, err = ioutil.ReadAll(r.Body)
	}
	if err != nil {
		fmt.Fprintln(loggerOutput, fmt.Sprintf("[ERROR] %v", fmt.Errorf("failed to read request %v", err)))
	}
	r.Body.Close()
	fmt.Fprintln(loggerOutput, fmt.Sprintf("[INFO] received %s", string(body)))
	w.Header().Set("Content-Type", "text/plain; charset=UTF-8")

	// TODO configurable cors origin
	w.Header().Set("Access-Control-Allow-Origin", "*")

	w.WriteHeader(http.StatusCreated)
	_, err = w.Write([]byte(""))
	if err != nil {
		fmt.Fprintln(loggerOutput, fmt.Sprintf("[ERROR] %v", fmt.Errorf("failed to write response %v", err)))
	}
}
